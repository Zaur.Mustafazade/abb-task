package az.abbbank.abbtask.enums;

public enum TaskStatus {
    NEW,
    IN_PROGRESS,
    DONE
}
