package az.abbbank.abbtask.enums;

public enum UserRole {
    ADMIN,
    USER
}
