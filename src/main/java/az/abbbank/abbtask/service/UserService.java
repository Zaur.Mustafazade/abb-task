package az.abbbank.abbtask.service;

import az.abbbank.abbtask.dto.request.UserReqDto;
import az.abbbank.abbtask.dto.respose.UserResponseDto;
import az.abbbank.abbtask.entity.User;
import az.abbbank.abbtask.repository.UserRepository;
import com.google.common.reflect.TypeToken;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;


@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;

    public List<UserResponseDto> findAllUsers(){
        List<User> userList = userRepository.findAll();
        Type listType = new TypeToken<List<UserResponseDto>>() {
        }.getType();

        return modelMapper.map(userList, listType);
    }

    public UserResponseDto createUser(UserReqDto dto) {
        User save = userRepository.save(modelMapper.map(dto, User.class));
        return modelMapper.map(save, UserResponseDto.class);
    }
}
