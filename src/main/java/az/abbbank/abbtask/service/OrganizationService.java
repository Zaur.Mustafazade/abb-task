package az.abbbank.abbtask.service;

import az.abbbank.abbtask.dto.request.OrganizationReqDto;
import az.abbbank.abbtask.dto.request.UserReqDto;
import az.abbbank.abbtask.dto.respose.OrganizationResponseDto;
import az.abbbank.abbtask.dto.respose.UserResponseDto;
import az.abbbank.abbtask.entity.Organization;
import az.abbbank.abbtask.entity.User;
import az.abbbank.abbtask.repository.OrganizationRepository;
import com.google.common.reflect.TypeToken;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;


@Service
@AllArgsConstructor
public class OrganizationService {
    private final OrganizationRepository organizationRepository;
    private final ModelMapper modelMapper;

    public List<OrganizationResponseDto> findAllOrganizations(){
        List<Organization> organizationList = organizationRepository.findAll();
        Type listType = new TypeToken<List<OrganizationResponseDto>>() {
        }.getType();

        return modelMapper.map(organizationList, listType);
    }

    public OrganizationResponseDto createOrganization(OrganizationReqDto dto) {
        Organization save = organizationRepository.save(modelMapper.map(dto, Organization.class));
        return modelMapper.map(save, OrganizationResponseDto.class);
    }

}
