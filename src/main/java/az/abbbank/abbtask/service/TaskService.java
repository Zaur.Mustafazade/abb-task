package az.abbbank.abbtask.service;

import az.abbbank.abbtask.dto.request.TaskReqDto;
import az.abbbank.abbtask.dto.respose.TaskResponseDto;
import az.abbbank.abbtask.entity.Task;
import az.abbbank.abbtask.repository.TaskRepository;
import com.google.common.reflect.TypeToken;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;


@Service
@AllArgsConstructor
public class TaskService {
    private final TaskRepository taskRepository;
    private final ModelMapper modelMapper;

    public List<TaskResponseDto> findAllTasks(){
        List<Task> taskList = taskRepository.findAll();
        Type listType = new TypeToken<List<TaskResponseDto>>() {
        }.getType();

        return modelMapper.map(taskList, listType);
    }

    public TaskResponseDto createTask(TaskReqDto dto) {
        Task save = taskRepository.save(modelMapper.map(dto, Task.class));
        return modelMapper.map(save, TaskResponseDto.class);
    }
}
