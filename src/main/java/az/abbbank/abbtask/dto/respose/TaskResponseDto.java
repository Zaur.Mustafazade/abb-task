package az.abbbank.abbtask.dto.respose;

import az.abbbank.abbtask.enums.TaskStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskResponseDto {

    private Long id;
    private String title;
    private String description;
    private String buildDate;
    private String assignedTo;
    private TaskStatus status;

    public TaskResponseDto(String title, String description, String buildDate, String assignedTo, TaskStatus status) {
        this.title = title;
        this.description = description;
        this.buildDate = buildDate;
        this.assignedTo = assignedTo;
        this.status = status;
    }
}
