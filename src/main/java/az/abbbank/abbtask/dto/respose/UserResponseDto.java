package az.abbbank.abbtask.dto.respose;

import az.abbbank.abbtask.entity.Organization;
import az.abbbank.abbtask.entity.Task;
import az.abbbank.abbtask.enums.UserRole;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserResponseDto {
    private Long id;
    private String name;
    private String surname;
    private String username;
    private String password;
    private String email;
    private UserRole userRole;
    private Organization organization;
    private List<Task> taskList = new ArrayList<>();

    public UserResponseDto(String name, String surname, String username, String password, String email, UserRole userRole,Organization organization, List<Task> taskList) {
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
        this.email = email;
        this.userRole = userRole;
        this.organization = organization;
        this.taskList = taskList;
    }

}
