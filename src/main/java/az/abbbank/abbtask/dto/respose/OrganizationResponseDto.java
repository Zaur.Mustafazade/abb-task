package az.abbbank.abbtask.dto.respose;

import az.abbbank.abbtask.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrganizationResponseDto {

    private Long id;
    private String name;
    private String phone;
    private String address;
    private String username;
    private String email;
    private String password;

    public OrganizationResponseDto(String name, String phone, String address, String username, String email, String password) {
        this.name = name;
        this.phone = phone;
        this.address = address;
        this.username = username;
        this.email = email;
        this.password = password;
    }
}
