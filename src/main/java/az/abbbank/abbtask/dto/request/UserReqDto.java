package az.abbbank.abbtask.dto.request;

import az.abbbank.abbtask.entity.Organization;
import az.abbbank.abbtask.entity.Task;
import az.abbbank.abbtask.enums.UserRole;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserReqDto {

    private String name;
    private String surname;
    private String username;
    private String password;
    private String email;
    private UserRole userRole;
    private Organization organization;
    private List<Task> taskList = new ArrayList<>();


}
