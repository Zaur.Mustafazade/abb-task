package az.abbbank.abbtask.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrganizationReqDto {

    private String name;
    private String phone;
    private String address;
    private String username;
    private String email;
    private String password;

}
