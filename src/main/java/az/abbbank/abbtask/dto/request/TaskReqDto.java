package az.abbbank.abbtask.dto.request;

import az.abbbank.abbtask.enums.TaskStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskReqDto {

    private String title;
    private String description;
    private String buildDate;
    private String assignedTo;
    private TaskStatus status;


}
