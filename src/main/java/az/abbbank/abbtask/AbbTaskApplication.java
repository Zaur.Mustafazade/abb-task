package az.abbbank.abbtask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AbbTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(AbbTaskApplication.class, args);
    }

}
