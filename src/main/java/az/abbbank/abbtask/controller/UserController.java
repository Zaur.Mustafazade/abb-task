package az.abbbank.abbtask.controller;

import az.abbbank.abbtask.dto.request.UserReqDto;
import az.abbbank.abbtask.dto.respose.UserResponseDto;
import az.abbbank.abbtask.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/users")
@AllArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping("/list")
    public ResponseEntity<List<UserResponseDto>> getAll(){
        return  ResponseEntity.status(HttpStatus.OK).body(userService.findAllUsers());
    }

    @PostMapping("/register")
    public ResponseEntity<UserResponseDto> createUser(@Valid @RequestBody UserReqDto userReqDto){
        UserResponseDto userResponseDto=userService.createUser(userReqDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(userResponseDto);
    }
}
