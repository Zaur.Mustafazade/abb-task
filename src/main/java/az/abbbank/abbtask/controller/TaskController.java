package az.abbbank.abbtask.controller;

import az.abbbank.abbtask.dto.request.TaskReqDto;
import az.abbbank.abbtask.dto.respose.TaskResponseDto;
import az.abbbank.abbtask.service.TaskService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/tasks")
@AllArgsConstructor
public class TaskController {
    private final TaskService taskService;

    @GetMapping("/allTasks")
    public ResponseEntity<List<TaskResponseDto>> getAll(){
        return  ResponseEntity.status(HttpStatus.OK).body(taskService.findAllTasks());
    }

    @PostMapping("/create")
    public ResponseEntity<TaskResponseDto> createTask(@Valid @RequestBody TaskReqDto taskReqDto){
        TaskResponseDto taskResponseDto=taskService.createTask(taskReqDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(taskResponseDto);
    }

}
