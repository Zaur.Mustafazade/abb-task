package az.abbbank.abbtask.controller;

import az.abbbank.abbtask.dto.request.OrganizationReqDto;
import az.abbbank.abbtask.dto.respose.OrganizationResponseDto;
import az.abbbank.abbtask.service.OrganizationService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/organizations")
@Slf4j
@AllArgsConstructor
public class OrganizationController {
    private final OrganizationService organizationService;

    @GetMapping("/allOrganizations")
    public ResponseEntity<List<OrganizationResponseDto>> getAll(){
        return  ResponseEntity.status(HttpStatus.OK).body(organizationService.findAllOrganizations());
    }

    @PostMapping("/create")
    public ResponseEntity<OrganizationResponseDto> createOrganizaton(@Valid @RequestBody OrganizationReqDto organizationReqDto){
        OrganizationResponseDto organizationResponseDto=organizationService.createOrganization(organizationReqDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(organizationResponseDto);
    }
}
