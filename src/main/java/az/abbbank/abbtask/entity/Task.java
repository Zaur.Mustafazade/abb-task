package az.abbbank.abbtask.entity;

import az.abbbank.abbtask.enums.TaskStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Table(name = "tasks")
@Entity
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="title",nullable = false)
    private String title;

    @Column(name="description",nullable = false)
    private String description;

    @Column(name="deadline",nullable = true)
    private String buildDate;

    @Column(name = "assignedTo",nullable = false)
    private String assignedTo;

    @Column(name = "status",nullable = false)
    @Enumerated(value = EnumType.STRING)
    private TaskStatus status;

    @ManyToOne
    @JsonIgnore
    @ToString.Exclude
    private User user;

    public Task(String title, String description, String buildDate, String assignedTo, TaskStatus status) {
        this.title = title;
        this.description = description;
        this.buildDate = buildDate;
        this.assignedTo = assignedTo;
        this.status = status;
    }

}
