package az.abbbank.abbtask.entity;

import az.abbbank.abbtask.enums.UserRole;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@Table(name = "users")
@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "username", nullable = false)
    private String username;

    @Transient
    @Column(name = "password",nullable = false)
    @Size(min = 6, message = "Password must contain at least 6 characters")
    private String password = "123456";

    @Column(name = "email",nullable = false)
    private String email;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "role",length = 20)
    private UserRole userRole;

    @ManyToOne
    private Organization organization;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private List<Task> taskList= new ArrayList<>();

    public User(String name, String surname, String username, String password, String email, UserRole userRole,Organization organization, List<Task> taskList) {
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
        this.email = email;
        this.userRole = userRole;
        this.organization = organization;
        this.taskList = taskList;
    }


}
