package az.abbbank.abbtask.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@Table(name = "organizations")
@Entity
public class Organization {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "phone")
    private String phone;

    @Column(name = "address")
    private String address;

    @Column(name = "username")
    private String username;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    @Size(min = 6, message = "Password must contain at least 6 characters")
    private String password;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "organization_id")
    @Fetch(FetchMode.JOIN)
    @ToString.Exclude
    private List<User> userList = new ArrayList<>();

    public Organization(String name, String phone, String address, String username, String email, String password) {
        this.name = name;
        this.phone = phone;
        this.address = address;
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public Organization(String name, String username, String email) {
        this.name = name;
        this.username = username;
        this.email = email;
    }
}
