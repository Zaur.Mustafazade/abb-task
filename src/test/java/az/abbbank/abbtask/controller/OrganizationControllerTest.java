package az.abbbank.abbtask.controller;

import az.abbbank.abbtask.dto.request.OrganizationReqDto;
import az.abbbank.abbtask.dto.respose.OrganizationResponseDto;
import az.abbbank.abbtask.service.OrganizationService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@WebMvcTest(OrganizationController.class)
public class OrganizationControllerTest {

    private static final String MAIN_URL= "/organizations";
    private static final String ORGANIZATION_CREATE= "/create";
    private static final String FIND_ALL= "/allOrganizations";

    private static OrganizationReqDto organizationReqDto;
    private  static List<OrganizationResponseDto> organizationResponseDtoList=new ArrayList<>();
    private static OrganizationResponseDto organizationResponseDto;

    private ObjectMapper objectMapper=new ObjectMapper();

    @BeforeAll
    static void setUp(){
        organizationResponseDto = new OrganizationResponseDto("test","test","test","test","test","123456");
        organizationResponseDtoList.add(organizationResponseDto);

        organizationReqDto = new OrganizationReqDto("test","test","test","test","test","123456");

    }

    @MockBean
    OrganizationService organizationService;

    @Autowired
    MockMvc mockMvc;

    @Test
    public  void testAllOrganizations() throws Exception {

        when(organizationService.findAllOrganizations()).thenReturn(organizationResponseDtoList);

        mockMvc.perform(get(MAIN_URL+FIND_ALL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(organizationResponseDtoList)));

    }

    @Test
    public  void testCreateOrganization() throws Exception {

        when(organizationService.createOrganization(organizationReqDto)).thenReturn(organizationResponseDto);

        mockMvc.perform(post(MAIN_URL+ORGANIZATION_CREATE)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(organizationReqDto))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andDo(print())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(organizationResponseDto)));
    }

}
