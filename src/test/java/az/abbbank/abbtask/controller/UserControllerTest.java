package az.abbbank.abbtask.controller;


import az.abbbank.abbtask.dto.request.UserReqDto;
import az.abbbank.abbtask.dto.respose.UserResponseDto;
import az.abbbank.abbtask.entity.Organization;
import az.abbbank.abbtask.entity.Task;
import az.abbbank.abbtask.enums.TaskStatus;
import az.abbbank.abbtask.enums.UserRole;
import az.abbbank.abbtask.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@WebMvcTest(UserController.class)

public class UserControllerTest {

     private static final String MAIN_URL= "/users";
     private static final String USER_REGISTER= "/register";
     private static final String FIND_ALL= "/list";

    private static UserReqDto userReqDto;
    private  static List<UserResponseDto> userResponseDaoList=new ArrayList<>();
    private static UserResponseDto userResponseDto;

    private ObjectMapper objectMapper=new ObjectMapper();

    @BeforeAll
    static void setUp(){
        userResponseDto = new UserResponseDto("Ayhan", "Huseynov", "ayhan", "1234560", "ayhan@mail.ru", UserRole.USER,
                (new Organization("admin","admin","admin")),
                List.of(new Task("test","test","02.03.22","test", TaskStatus.NEW)));
        userResponseDaoList.add(userResponseDto);

        userReqDto = new UserReqDto("Ayhan", "Huseynov", "ayhan", "1234560", "ayhan@mail.ru", UserRole.USER,
                (new Organization("admin","admin","admin")),
                List.of(new Task("test","test", "02.03.22","test", TaskStatus.NEW)));
    }

    @MockBean
    UserService userService;

    @Autowired
    MockMvc mockMvc;

    @Test
    public  void testAllUsers() throws Exception {

        when(userService.findAllUsers()).thenReturn(userResponseDaoList);

        mockMvc.perform(get(MAIN_URL+FIND_ALL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(userResponseDaoList)));

    }

    @Test
    public  void testCreateUser() throws Exception {

        when(userService.createUser(userReqDto)).thenReturn(userResponseDto);

        mockMvc.perform(post(MAIN_URL+USER_REGISTER)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(userReqDto))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andDo(print())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(userResponseDto)));
    }
}

