package az.abbbank.abbtask.controller;

import az.abbbank.abbtask.dto.request.TaskReqDto;
import az.abbbank.abbtask.dto.respose.TaskResponseDto;
import az.abbbank.abbtask.enums.TaskStatus;
import az.abbbank.abbtask.service.TaskService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@WebMvcTest(TaskController.class)

public class TaskControllerTest {
    private static final String MAIN_URL= "/tasks";
    private static final String TASK_CREATE= "/create";
    private static final String FIND_ALL= "/allTasks";

    private static TaskReqDto taskReqDto;
    private  static List<TaskResponseDto> taskResponseDaoList=new ArrayList<>();
    private static TaskResponseDto taskResponseDto;

    private ObjectMapper objectMapper=new ObjectMapper();

    @BeforeAll
    static void setUp(){
        taskResponseDto = new TaskResponseDto("test","test", "02.03.22","test", TaskStatus.NEW);
        taskResponseDaoList.add(taskResponseDto);

        taskReqDto = new TaskReqDto("test","test","02.03.22","test",TaskStatus.NEW);

    }

    @MockBean
    TaskService taskService;

    @Autowired
    MockMvc mockMvc;

    @Test
    public  void testAllTasks() throws Exception {

        when(taskService.findAllTasks()).thenReturn(taskResponseDaoList);

        mockMvc.perform(get(MAIN_URL+FIND_ALL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(taskResponseDaoList)));

    }

    @Test
    public  void testCreateTask() throws Exception {

        when(taskService.createTask(taskReqDto)).thenReturn(taskResponseDto);

        mockMvc.perform(post(MAIN_URL+TASK_CREATE)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(taskReqDto))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andDo(print())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(taskResponseDto)));
    }
}
