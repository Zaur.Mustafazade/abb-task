package az.abbbank.abbtask.service;

import az.abbbank.abbtask.dto.request.TaskReqDto;
import az.abbbank.abbtask.dto.respose.TaskResponseDto;
import az.abbbank.abbtask.entity.Task;
import az.abbbank.abbtask.enums.TaskStatus;
import az.abbbank.abbtask.repository.TaskRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class TaskServiceTest {

    @Mock
    TaskRepository taskRepository;

    @InjectMocks
    TaskService taskService;

    @Spy
    ModelMapper modelMapper;

    private static TaskResponseDto taskResponseDto;
    private static Task task;
    private static TaskReqDto taskReqDto;

    @BeforeAll
    static void setUp() {
        taskResponseDto = new TaskResponseDto("test","test", "02.03.22","test", TaskStatus.NEW);

        task = new Task("test", "test", "02.03.22",  "test", TaskStatus.NEW);

        taskReqDto = new TaskReqDto("test","test","02.03.22","test",TaskStatus.NEW);
    }

    @Test
    public void testAllTasks() {
        List<TaskResponseDto> taskResponseDtoList = new ArrayList<>();
        taskResponseDtoList.add(taskResponseDto);
        Task task = modelMapper.map(taskResponseDto, Task.class);
        List<Task> listType = new ArrayList<>();
        listType.add(task);
        given(taskRepository.findAll()).willReturn(listType);

        assertEquals(taskResponseDtoList, taskService.findAllTasks());

        verify(taskRepository, times(1)).findAll();

    }

    @Test
    public void testCreateTask() {

        when(taskRepository.save(modelMapper.map(taskReqDto, Task.class))).thenReturn(task);

        assertThat(taskService.createTask(taskReqDto)).isEqualTo(taskResponseDto);

        verify(taskRepository, atLeast(1)).save(modelMapper.map(taskReqDto, Task.class));
    }
}