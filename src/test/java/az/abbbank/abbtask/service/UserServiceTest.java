package az.abbbank.abbtask.service;


import az.abbbank.abbtask.dto.request.UserReqDto;
import az.abbbank.abbtask.dto.respose.UserResponseDto;
import az.abbbank.abbtask.entity.Organization;
import az.abbbank.abbtask.entity.Task;
import az.abbbank.abbtask.entity.User;
import az.abbbank.abbtask.enums.TaskStatus;
import az.abbbank.abbtask.enums.UserRole;
import az.abbbank.abbtask.repository.UserRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)

public class UserServiceTest {

        @Mock
        UserRepository userRepository;

        @InjectMocks
        UserService userService;

        @Spy
        ModelMapper modelMapper;

        private static UserResponseDto userResponseDto;
        private static User user;
        private static UserReqDto userReqDto;

        @BeforeAll
        static void setUp() {

            user = new User("Ayhan", "Huseynov", "ayhan", "1234560", "ayhan@mail.ru", UserRole.USER,
                    (new Organization("admin","admin","admin")),
                    List.of(new Task("test","test", "02.03.22","test", TaskStatus.NEW)));

            userResponseDto = new UserResponseDto("Ayhan", "Huseynov", "ayhan", "1234560", "ayhan@mail.ru", UserRole.USER,
                    (new Organization("admin","admin","admin")),
                    List.of(new Task("test","test",  "02.03.22","test", TaskStatus.NEW)));

            userReqDto = new UserReqDto("Ayhan", "Huseynov", "ayhan", "1234560", "ayhan@mail.ru", UserRole.USER,
                    (new Organization("admin","admin","admin")),
                    List.of(new Task("test","test",  "02.03.22","test", TaskStatus.NEW)));
        }

        @Test
        public void testAllUsers() {
            List<UserResponseDto> userResponseDtoList = new ArrayList<>();
            userResponseDtoList.add(userResponseDto);
            User user = modelMapper.map(userResponseDto, User.class);
            List<User> listType = new ArrayList<>();
            listType.add(user);
            given(userRepository.findAll()).willReturn(listType);

            assertEquals(userResponseDtoList, userService.findAllUsers());

            verify(userRepository, times(1)).findAll();

        }

        @Test
        public void testCreateUser() {

            when(userRepository.save(modelMapper.map(userReqDto, User.class))).thenReturn(user);

            assertThat(userService.createUser(userReqDto)).isEqualTo(userResponseDto);

            verify(userRepository, atLeast(1)).save(modelMapper.map(userReqDto, User.class));
        }
}

