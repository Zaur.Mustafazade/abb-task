package az.abbbank.abbtask.service;

import az.abbbank.abbtask.dto.request.OrganizationReqDto;
import az.abbbank.abbtask.dto.respose.OrganizationResponseDto;
import az.abbbank.abbtask.entity.Organization;
import az.abbbank.abbtask.repository.OrganizationRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)

public class OrganizationServiceTest {

    @Mock
    OrganizationRepository organizationRepository;

    @InjectMocks
    OrganizationService organizationService;

    @Spy
    ModelMapper modelMapper;

    private static OrganizationResponseDto organizationResponseDto;
    private static Organization organization;
    private static OrganizationReqDto organizationReqDto;

    @BeforeAll
    static void setUp() {

        organization = new Organization("test","test","test","test","test","123456");

        organizationResponseDto = new OrganizationResponseDto("test","test","test","test","test","123456");

        organizationReqDto = new OrganizationReqDto("test","test","test","test","test","123456");
    }

    @Test
    public void testAllOrganizatons() {
        List<OrganizationResponseDto> organizationResponseDtoList = new ArrayList<>();
        organizationResponseDtoList.add(organizationResponseDto);
        Organization organization = modelMapper.map(organizationResponseDto, Organization.class);
        List<Organization> listType = new ArrayList<>();
        listType.add(organization);
        given(organizationRepository.findAll()).willReturn(listType);

        assertEquals(organizationResponseDtoList, organizationService.findAllOrganizations());

        verify(organizationRepository, times(1)).findAll();

    }

    @Test
    public void testCreateOrganization() {

        when(organizationRepository.save(modelMapper.map(organizationReqDto, Organization.class))).thenReturn(organization);

        assertThat(organizationService.createOrganization(organizationReqDto)).isEqualTo(organizationResponseDto);

        verify(organizationRepository, atLeast(1)).save(modelMapper.map(organizationReqDto, Organization.class));


    }
}


